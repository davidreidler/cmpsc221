/*******************************************************************************
 * Author:      David Reidler                                                  *
 * Email:       dir5187@psu.edu                                                *
 * Assignment:  1                                                              *
 * Description: This is the first implementation of the date class. This class *
 * uses 3 integers to store the day, month and year associated with a date.    *
 ******************************************************************************/

public class DateInts {

	/* Data for first implementation */
	private int month;
	private int day;
	private int year;

	/* Constructors */
	DateInts(int month, int day, int year){
		/* The constructor assumes that the arguments are correct */
		this.month = month;
		this.day   = day;
		this.year  = year;
	}


	/* Getters */
	public int getDay(){
		return day;
	}

	public int getMonth(){
		return month;
	}

	public int getYear(){
		return year;
	}


	/* Setters */
	public void setDay(int day){
		/* Each month has at most 31 days */
		if(day <= 31 && day > 0)
			this.day = day;
		else this.day = 1;
	}

	public void setMonth(int month){
		/* Every year has 12 months */
		if(month <= 12 && month > 0)
			this.month = month;
		else this.month = 1;
	}

	public void setYear(int year){
		/* Years must be positive */
		if(year > 0)
			this.year = year;
		else this.year = 2000;
	}


	/* Member Methods */
	public boolean equals(DateInts date){
		/* Compare all values for similarity */
		return (this.day == date.getDay()     &&
				this.month == date.getMonth() && 
				this.year == date.getYear());
	}

	public int compareTo(DateInts date){
		/* Weights for the difference between two dates */
		return ((this.year - date.getYear()) * 144 +
				(this.month - date.getMonth()) * 12 +
				(this.day - date.getDay()));
	}

	public String toString(){
		return (Integer.toString(this.day)   + "/" +
				Integer.toString(this.month) + "/" +
				Integer.toString(this.year));
	}
}
