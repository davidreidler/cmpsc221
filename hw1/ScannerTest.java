/*******************************************************************************
 * Author:      David Reidler                                                  *
 * Email:       dir5187@psu.edu                                                *
 * Assignment:  1                                                              *
 * Description: This program demonstrates the Scanner's downfall with a single *
 * string combined with next() and nextInt(). If the input "How are you?" is   *
 * given to the scanner during next() call, the program will throw an          *
 * exception when it comes to nextInt().                                       *
 ******************************************************************************/

import java.util.Scanner;
public class ScannerTest {
	public static void main(String[] params){
		String word;
		int x;
		double y;

		Scanner input = new Scanner(System.in);
		System.out.println("Please enter a word: ");
		word = input.next();

		System.out.println("Please enter an integer value: ");
		x = input.nextInt();

		System.out.println("Please enter a double value: ");
		y = input.nextDouble();

		System.out.printf("The three values entered are: \"%s\", %d, and %f.\n", word, x, y);

		return;
	}
}
