/*******************************************************************************
 * Author:      David Reidler                                                  *
 * Email:       dir5187@psu.edu                                                *
 * Assignment:  1                                                              *
 * Description: This program takes a year and a day index and output the       *
 * entire calendar for the year.                                               *
 ******************************************************************************/

import java.util.Scanner;
public class Calendar {
	/* Variables that are used throughout the class */
	private static int year = 0;
	private static int firstDay = 0;

	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);

		System.out.print("Enter a year: ");
		year = scanner.nextInt();

		System.out.print("Enter the first day of the year: ");
		firstDay = scanner.nextInt();

		if(firstDay >= 7 || year < 0){
			System.out.println("Invalid input. Check your index and year");
			return;
		}

		/* Print out calendar */
		for(int i = 1; i <= 12; i++){
			firstDay = printMonth(i, firstDay );
		}	

		return;
	}

	private static int printMonth(int month, int weekDayIndex){
		String mon = getMonth(month);
		int days = getDays(month);

		System.out.println("\n" +  "\t " + mon + " " + 
						   Integer.toString(year));
		System.out.println("-----------------------------");
		System.out.println(" Sun Mon Tue Wed Thu Fri Sat ");

		/* Print the correct amount of spaces before printing the next day */
		for(int i = 0; i < weekDayIndex; i++){
			System.out.print("    ");
		}

		/* Loop for each day of the month */
		for(int curDay = 1; curDay <= days; curDay++){
			System.out.printf("%1$4d", curDay);	

			weekDayIndex++;

			/* Shift the day index back to 0 when its at the end of the week */
			if(weekDayIndex == 7){
				weekDayIndex = 0;
				System.out.println();
			} 
		}

		/* Print a newline so that the next month starts on a newline */
		if(weekDayIndex != 0)
			System.out.println();

		return weekDayIndex;
	}

	/* This method returns the Month based on the integer given */
	private static String getMonth(int month){
		switch (month){
			case 1: return "January";
			case 2: return "Febuary";
			case 3: return "March";
			case 4: return "April";
			case 5: return "May";
			case 6: return "June";
			case 7: return "July";
			case 8: return "August";
			case 9: return "September";
			case 10: return "October";
			case 11: return "Novemember";
			case 12: return "December";
			default: return "Invalid";
		}
	}

	/* This function gets the number of days in the month */ 
	private static int getDays(int month){
		switch(month){
			case 2: return 28; /* We aren't worried about leap years */
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12: return 31;
			default: return 30;
		}
	}
}
