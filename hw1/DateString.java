/*******************************************************************************
 * Author:      David Reidler                                                  *
 * Email:       dir5187@psu.edu                                                *
 * Assignment:  1                                                              *
 * Description: This is the second implementation of a date class. This class  *
 * uses a single string to store the date. The string consists of the          *
 * following format: day/month/year                                            *
 ******************************************************************************/

public class DateString {
	/* Only one instance variable needed */
	private String date;

	/* Constructors */
	public DateString(){
		/* Default date so that each object matches regex */
		date = new String("1/January/2000");
	}

	public DateString(int day, int month, int year){
		date = new String(Integer.toString(day)   + "/" + 
						  Integer.toString(month) + "/" + 
						  Integer.toString(year));
	}


	/* Getters */
	public String getDay(){
		return date.split("/")[0];
	}

	public String getMonth(){
		return date.split("/")[1];
	}

	public String getYear(){
		return date.split("/")[2];
	}


	/* Setters */
	public void setDay(int day){
		/* String representation of our day */
		String newDay = "1";

		/* Data to remove from the date string */
		String oldDay = date.split("/")[0];

		if(day <= 31 && day > 0)
			/* Replaces the first instance of the day */
			newDay = Integer.toString(day);

		date = date.replaceFirst(oldDay, newDay);
	}

	public void setMonth(int month){
		/* Find the old month and the new month */
		String newMonth = (month > 0 && month <= 12) ? Integer.toString(month) :
													  "1";
		/* Break up the string and save each part in an array */
		String[] dateParams = date.split("/");

		/* Create a new string based on the new month */
		dateParams[1] = new String(newMonth);

		/* Destroy the old string and recreate it */
		date = "".concat(dateParams[0] + "/" + 
					     dateParams[1] + "/" +
						 dateParams[2]);
	}

	public void setYear(int year){
		/* Find the old year and the new year */
		String newYear = "2000";
		String oldYear = date.split("/")[2];

		/* Year restrictions */
		if(year > 0 && year < Integer.MAX_VALUE)
			newYear = Integer.toString(year);

		/* Write changes to variable */
		date = date.replaceFirst(oldYear, newYear);
	}


	/* Misc Functions */

	/* Return the built in comparesTo() an equals() for strings */
	public boolean equals(DateString date){
		return this.date.equals(date.date);
	}	

	public int compareTo(DateString otherDate){
		return this.date.compareTo(otherDate.date);
	}

	/* Just return a copy of the string */
	public String toString(){
		return date;
	}
}
