/*******************************************************************************
 * Author:      David Reidler                                                  *
 * Email:       dir5187@psu.edu                                                *
 * Assignment:  1                                                              *
 * Description: This class contains test cases for both the DateInts class and *
 * the DateString class. I've attemped to test each method on simple objects   *
 ******************************************************************************/

public class DateTest{
	/* Program entry point */
	public static void main(String[] args){
		/* Test DateInts Class - Implementation with Integers */
		DateInts date1 = new DateInts(1,1, 2014);  // January 1st, 2014
		
		/* Demonstrate Setter Methods */
		System.out.println("Testing setter methods...");

		System.out.println("Changing day to 15.");
		date1.setDay(15);

		System.out.println("Changing month to 3.");
		date1.setMonth(3);

		System.out.println("Changing month to 1999.");
		date1.setYear(1999);

		/* Demonstrates Getter Methods */
		System.out.println("\nTesting getter methods...");

		System.out.printf("The day is %d.\n", date1.getDay());
		System.out.printf("The month is %d.\n", date1.getMonth());
		System.out.printf("The year is %d.\n", date1.getYear());

		System.out.println("Output of toString() method:");
		System.out.println(date1.toString());

		/* Comparing two dates */
		DateInts date2 = new DateInts(2,14,2016);  // February 14th, 2016	
		System.out.printf("\ndate1.compareTo(date2) == %d\n", 
							date1.compareTo(date2));  // March 15th, 1999

		System.out.println(date1.equals(date2) ? "They are the same date" : 
												 "They are different dates");

		/* Testing DateString Implementation - uses a single String */
		DateString date3 = new DateString(1, 1, 2014);

		date3.setMonth(2);
		date3.setYear(1995);
		date3.setDay(13);

		System.out.println("Output of date3.toString(): ");
		System.out.println(date3.toString());

		/* compareTo() and equals() and toString() don't need to be tested *
		 * since they use java's built in methods                          */
	
		System.out.println("\nAll done with test cases.");
		return;	
	}
} 
