/******************************************************************************
 * BankDatabase.java                                                          *
 * Author:		David Reidler                                                 *
 * Email: 		dir5187@psu.edu                                               *
 * Assignment:  5                                                             *
 * Description: This class will provide static methods to access user account * 
 * information. It is the only public non-abstract class available in the     *
 * package, to contain instances of account objects while still allowing an   *
 * interface to program with.                                                 *
 *****************************************************************************/
package ATIM;
import java.io.*;
import java.util.*;
public class BankDatabase {

	/* Maps account numbers to Accounts */
	private static HashMap<Integer, Account> accounts;

	/* Maps account numbers to Customers */
	private static HashMap<Integer, Customer> customers;


	public static boolean accountExists(int accNumber) {
		return accounts.get(accNumber) != null;	
	}

	public static boolean verifyPIN(int accNumber, int PIN) {
		Account a = accounts.get(accNumber);
		if(a.getPin() == PIN) return true;
		else return false; 
	}

	private static int getAccountType(Account a) {
		String name = a.getClass().getName();
		switch(name){
			case "ATIM.StudentAccount": return 1;
			case "ATIM.BasicAccount": return 2;
			case "ATIM.UnlimitedAccount": return 3;
			default: return 0;
		}
	}
	
	public static void init() {
		/* Load text files if not already loaded */
		if(accounts == null) loadAccounts();
		if(customers == null) loadCustomers();
		return;
	}

	public static void saveState() {
		saveAccounts();
		saveCustomers();
	}
	
	private static void loadAccounts() {
		String nextLine;
		accounts = new HashMap<Integer, Account>();

		try {
			/* Open the reader */
			BufferedReader reader = new BufferedReader(new FileReader("accounts.txt"));
			while((nextLine = reader.readLine()) != null) {
				parseAccountLine(nextLine);
			}
			
			/* Close the reader */
			reader.close();
		} catch (Exception e) {
			System.out.println("ERROR IN ACCOUNTS!");
		} 
	} 
	private static void loadCustomers() {
		String nextLine;
		customers = new HashMap<Integer, Customer>();

		try {
			/* Open the reader */
			BufferedReader reader = new BufferedReader(new FileReader("users.txt"));
			while ((nextLine = reader.readLine()) != null){
				/* Parse the line and save data */
				parseUserLine(nextLine);
			}
			
			/* Close the reader */
			reader.close();
		} catch (Exception e) {
			System.out.println("ERROR IN USERS!");
		}
	}

	/* Type; AccountNumber; Pin; AvailableBalance */
	private static void parseAccountLine(String nextLine) {
		Account result;
		String[] parts = nextLine.split(";");

		/* Parse data */
		int type = Integer.parseInt(parts[0]);
		int aNumber = Integer.parseInt(parts[1]);
		int pin = Integer.parseInt(parts[2]);
		double curBalance = Double.parseDouble(parts[3]);
		
		/* Create the correct account type */
		switch(type) {
			case 1: result = new StudentAccount(aNumber, pin, curBalance); break;
			case 2: result = new BasicAccount(aNumber, pin, curBalance); break;
			case 3: result = new UnlimitedAccount(aNumber, pin, curBalance); break;
			default: result = null; break;
		}

		/* Save the Account reference */
		accounts.put(aNumber, result);
	}

	/* FullName; AccountNumber; Address; PhoneNumber */
	private static void parseUserLine(String nextLine) {
		Customer result;
		String[] parts = nextLine.split(";");

		/* Parse data */
		int accNumber = Integer.parseInt(parts[1]);
	
		/* Encapsulate Customer information */
		result = new Customer(parts[0], parts[2], parts[3], Integer.parseInt(parts[1]));
	
		customers.put(accNumber, result);
	}

	private static void saveAccounts() {
		FileWriter writer;
		StringBuffer nextLine = new StringBuffer();
		try {
			/* Open a new writer */
			writer = new FileWriter("accounts.txt", false);
		
			/* Create the next line to be entered */
			for(Account a : accounts.values()) {
				/* Clear the buffer */
				nextLine.delete(0, nextLine.length());
				
				nextLine.append(getAccountType(a))
						.append(";")
						.append(a.getAccountNumber())
						.append(";")
						.append(a.getPin())
						.append(";")
						.append(a.getBalance())
						.append('\n');
				
				writer.write(nextLine.toString());
			}
			
			/* Write the string to writer */
			writer.close();
		} catch (IOException ioe) {
			System.out.println("There was a System Error, please inform the bank.");
		}
	}
	private static void saveCustomers() {
		FileWriter writer;
		StringBuffer nextLine = new StringBuffer();
		
		try {
			/* Create next line and write it to the writer */
			writer = new FileWriter("users.txt", false);
			for(Customer c : customers.values()){
				nextLine.delete(0, nextLine.length());
				
				nextLine.append(c.getFullName())
						.append(';')
						.append(c.getAccountNumber())
						.append(';')
						.append(c.getAddress())
						.append(';')
						.append(c.getPhoneNumber())
						.append('\n');
				
				
				writer.write(nextLine.toString());
			}
			
			/* Explicitly write the data and close the stream */
			writer.close();
		} catch (IOException ioe) {
			System.out.println("There was a System error, please inform the bank.");
		}
	}

	public static Customer findCustomer(int accountNumber) {
		return customers.get(accountNumber);
	}
	public static int getSpendingLimit(int accountNumber) {
		return accounts.get(accountNumber).spendingLimit();
	}
	public static void withdrawlFunds(int accNumber, int amount) {
		Account a = accounts.get(accNumber);
		
		if(amount <= a.spendingLimit()) {
			a.withdrawlFunds((double)amount);
		}
		return;
	}
	public static void depositFunds(int accNumber, double amount) {
		Account a = accounts.get(accNumber);
		a.depositFunds(amount);
	}
	public static double getBalance(int accountNumber) {
		return accounts.get(accountNumber).getBalance();
	}
	public static double getAvailableBalance(int accountNumber) {
		return accounts.get(accountNumber).getAvailableBalance();
	}

}
