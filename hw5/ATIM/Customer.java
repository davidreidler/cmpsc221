/******************************************************************************
 * Customer.java                                                              *
 *                                                                            *
 * Author: 		David Reidler                                                 *
 * E-Mail:		dir5187@psu.edu                                               *
 * Assignment: 	5                                                             *
 * Description: This file contains the implementation of a customer. This     *
 *  class stores customer information such as the customer's full name,       *
 *  address, account number, and their phone number.                          *
 *****************************************************************************/
package ATIM;
public class Customer {

	/* Fields */
	private String fullName;

	private String address;

	private String phoneNumber;

	private int accountNumber;

	/* Constructor */
	Customer(String name, String addr, String phone, int accNumber) {
		fullName = new String(name);
		address = new String(addr);
		phoneNumber = new String(phone);
		accountNumber = accNumber;
	}


	/* Getters */
	public int getAccountNumber() {
		return accountNumber;
	}
	
	public String getAddress() {
		return address;
	}

	public String getFullName() {
		return fullName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	/* Setters */
	void setAccountNumber(int newNumber) {
		accountNumber = newNumber;
	}
	
	void setAddress(String newAddr) {
		address = new String(newAddr);
	}

	void setFullName(String newName) {
		fullName = new String(newName);
	}

	void setPhoneNumber(String newNumber) {
		phoneNumber = new String(newNumber);
	}
}
