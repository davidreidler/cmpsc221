/******************************************************************************
 * CashDispenser.java                                                         *
 *                                                                            *
 * Author: 		David Reidler                                                 *
 * E-Mail:		dir5187@psu.edu                                               *
 * Assignment: 	5                                                             *
 * Description: This file contains the implementation of a cash dispenser. A  *
 * cash dispenser contains $20 and $10 bills and will distribute them out of  *
 * the ATM as the ATM deems necessary.                                        *
 *****************************************************************************/
package ATIM;
public class CashDispenser {
	private int twentyBills;
	private int tenBills;


	/* Constructor */	
	public CashDispenser() {
		twentyBills = 300;
		tenBills = 150;
	}

	public CashDispenser(int twenties, int tens) {
		twentyBills = twenties;
		tenBills = tens;
	}


	/* Methods */
	/* Returns the amount of money removed from the dispenser */
	public int removeCash(int amount) {
		int result = 0;

		while (amount != 0) {
			if(amount >= 20) {
				if(twentyBills > 0) {
					amount -= 20;
					result += 20;
					twentyBills--;
				} else if(tenBills > 0) {
					amount -= 10;
					result += 10;
					tenBills--;
				} else return result;
			} else {
				if(tenBills > 0) {
					tenBills--;
					amount -= 10;
					result += 10;
				} else return result;
			}
		}
		return result;
	}
	
	public int amountLeft() {
		return (20 * twentyBills) + (10 * tenBills);
	}
	public boolean empty() {
		return twentyBills == 0 && tenBills == 0;
	}

}
