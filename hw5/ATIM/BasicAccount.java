/******************************************************************************
 * BasicAccount.java                                                          *
 * Author:		David Reidler                                                 *
 * Email:		di5187@psu.edu                                                *
 * Assignment:  5                                                             *
 * Description: This file contains implementation for a basic account with    *
 * the bank. This type has a spending limit of $1000.                         *
 *****************************************************************************/

package ATIM;
class BasicAccount extends Account {
	private static final int SPENDING_LIMIT = 1000;

	BasicAccount() {
		super(0000, 1234, 0.0);
	}

	BasicAccount(int accNumber, int pinNumber, double curBalance) {
		super(accNumber, pinNumber, curBalance);
	}

	int spendingLimit() {
		return SPENDING_LIMIT;
	}
}
