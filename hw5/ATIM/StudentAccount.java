/******************************************************************************
 * Customer.java                                                              *
 *                                                                            *
 * Author: 		David Reidler                                                 *
 * E-Mail:		dir5187@psu.edu                                               *
 * Assignment: 	5                                                             *
 * Description: This file contains the implementation of a student account    *
 * with the bank. This account type has a spending limit of $200.             *
 *****************************************************************************/
package ATIM;
public class StudentAccount extends Account {
	private static final int SPENDING_LIMIT = 200;

	StudentAccount() {
		super(0000, 1234, 0.0);
	}
	
	StudentAccount(int accNumber, int PIN, double balance) {
		super(accNumber, PIN, balance);
	}
	
	int spendingLimit() {
		return SPENDING_LIMIT;
	}

}
