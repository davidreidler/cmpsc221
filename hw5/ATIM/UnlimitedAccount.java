/******************************************************************************
 * Customer.java                                                              *
 *                                                                            *
 * Author: 		David Reidler                                                 *
 * E-Mail:		dir5187@psu.edu                                               *
 * Assignment: 	5                                                             *
 * Description: This file contains the implementation of an account with the  *
 * bank that has no spending limit.                                           *
 *****************************************************************************/
package ATIM;
public class UnlimitedAccount extends Account {
	private static final int SPENDING_LIMIT = Integer.MAX_VALUE;

	UnlimitedAccount() {
		super(0000, 1234, 0.0);
	}
	
	UnlimitedAccount(int accNumber, int PIN, double balance) {
		super(accNumber, PIN, balance);
	}
	
	int spendingLimit() {
		// TODO Auto-generated method stub
		return SPENDING_LIMIT;
	}

}
