/******************************************************************************
 * ATM.java                                                                   *
 *                                                                            *
 * Author: 		David Reidler                                                 *
 * E-Mail:		dir5187@psu.edu                                               *
 * Assignment: 	5                                                             *
 * Description: This file contains the implementation for the ATM. This ATM   *
 * is mostly event driven. The program flows like a normal even driven        *
 * program but has more of a stage design in mind. Not all of the code        *
 * written within this file is owned by me, as the instructor gave me a       *
 * pre-written file which outlines the GUI layout. You can find the code      *
 * that he wrote on ANGEL. However, any modifications done to his original    *
 * work were done exclusively by myself. Please email me if you have any      *
 * problems with this program.                                                *
 *****************************************************************************/
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;

import ATIM.Customer;
import ATIM.CashDispenser;
import ATIM.BankDatabase;

public class ATM extends JFrame implements KeyListener
{
	public static enum STAGES { Welcome, Account, PIN, UserMain, Balance, Withdrawl, Deposit, PrintPrompt, Error };
	private STAGES currentStage = STAGES.Welcome;
	
    private JTextArea screen;
    
    private Customer customer;
    private CashDispenser dispenser;
    private int withdrawlAmount = 0;
    private int depositAmount = 0;
    
    private String input = new String();
    private boolean readingInput = false;
    
    private boolean allowPrinting = false;
    
    private int accountNumber;
    private int pin;
    private String printMessage;
    public ATM()
    {
        super("ATM");
        addKeyListener(this);
        
        /* Create the CashDispenser */
        dispenser = new CashDispenser();
        
        /* Initialize the BankDatabase */
        BankDatabase.init();
        
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        // Add the keypad panel to the frame
        ButtonsPanel buttonPanel = new ButtonsPanel(this);
        add(buttonPanel, BorderLayout.CENTER);

        // Add the control buttonsl to the frame
        ControlPanel control = new ControlPanel(this);
        add(control, BorderLayout.EAST);
    
        // Add the screen (represented as a Label) to the frame
        screen = new JTextArea("Welcome to Credit Union\nPress the Enter button to continue.");
        screen.setOpaque(true);
        screen.setBackground(Color.BLACK);
        screen.setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED));
        screen.setForeground(Color.GREEN);
        screen.setPreferredSize(new Dimension(250,200));
        screen.setEditable(false);
        screen.setLineWrap(true);
        screen.addKeyListener(this);
        add(screen, BorderLayout.NORTH);

        pack();
        setVisible(true);
        
        /* This listener will save the database state when the JFrame is closed */
        addWindowListener(new WindowAdapter() {
        	public void windowClosing(WindowEvent we) {
        		BankDatabase.saveState();
        	}
        });
    }

    /* Menu Printing */
	private void printMenu() {
		setScreenText("Welcome " + customer.getFullName() +
					   "\nPlease choose an option:" +
					   "\n  (1) Balance Inquiry" +
					   "\n  (2) Withdrawl Money" +
					   "\n  (3) Deposit Money" +
					   "\n  (4) Sign Out");
    }
	
	private void printWithdrawlMenu() {
    	StringBuilder sb = new StringBuilder();
		sb.append("How much would you like to withdrawl?");
		sb.append("\n (1) $20\t(4) $100");
		sb.append("\n (2) $50\t(5) $150");
		sb.append("\n (3) $70\t(6) $200");
		sb.append("\n (0) Exit\n\n");
		
		setScreenText(sb.toString());
    }
    
	private void printDepositMenu() {
    	setScreenText("How much would you like to deposit? ");
    }
    
	/* Stage Manipulation */
	
    /* Note about program flow.
     * 
     * This program relies around different "program stages". For example 
     * When the user is at the main menu, they are in the "UserMain" stage,
     * where they can choose a transaction. Each Stage represents a different
     * state in which the user can interact with the program. For example,
     * the user can not Print a reciept in the main menu, because there is
     * nothing to print at the main menu!
     * 
    */
	
	/* This function will change the program stage based on the program's
	 * current stage
	 */
    public void advanceStage() {
    	int choice;
    	
    	/* The program should have gotten some kind of input */
    	if(isReading() && input.equals("")) {
    		setStage(currentStage);
    		return;
    	}
    	
    	/* Do something cool with the input! */
    	/* Then change to the next stage */
    	switch (currentStage) {
    		case Error:
    			setStage(STAGES.Welcome);
    			break;
	    	case Welcome:
	    		setStage(STAGES.Account);
	    		break;
	    		
	    	case Account:
	    		accountNumber = Integer.parseInt(input);
	    		if(BankDatabase.findCustomer(accountNumber) == null) {
	    			setError("We could not find that Account.");
	    		} else 
	    			setStage(STAGES.PIN);
	    		break;
	    		
	    	case PIN:
	    		this.pin = Integer.parseInt(input);
	    		if(!BankDatabase.verifyPIN(accountNumber, pin)) {
	    			setError("That PIN is not correct for this account.\nPress Enter or Exit to quit");
	    		} else {
	    			this.customer = BankDatabase.findCustomer(this.accountNumber);
	    			setStage(STAGES.UserMain);
	    		}
	    		break;
	    	case UserMain:
	    		choice = Integer.parseInt(input);
	    		switch(choice) {
		    		case 1:
		    			setStage(STAGES.Balance);
		    			advanceStage();
		    			break;
		    		case 2:
		    			setStage(STAGES.Withdrawl);
		    			break;
		    		case 3:
		    			setStage(STAGES.Deposit);
		    			break;
		    		case 4:
		    			setStage(STAGES.Welcome);
		    			break;
		    		case 99:
		    			/* Invokes a new Window Event to close the form */
		    			/* This is the 99 Kill Switch */
		    			dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
		    			break;
	    			default:
	    				setError("Invalid selection!", STAGES.UserMain);
	    		}
	    		break;
	    	case Balance:
	    		generatePrintMessage();
	    		setStage(STAGES.PrintPrompt);
	    		break;
	    	case Withdrawl:
	    		int amount = 0;
	    		choice = Integer.parseInt(input);
	    		/* Parse Selection */
	    		if(choice == 0) {
	    			setStage(STAGES.UserMain);
	    			break;
	    		} else if(choice > 0 && choice <= 6) {
	    			amount = parseWithdrawlAmount(choice);
	    		} else { 
	    			setError("That is not a valid selection.", STAGES.Withdrawl);
	    			break;
	    		}
	    		
	    		/* Make sure they can withdrawl that amount */
	    		if(dispenser.amountLeft() < withdrawlAmount) {
	    			setError("This ATM does not have that much money.\nCannot process transaction.", STAGES.Withdrawl);
	    		} else if(withdrawlAmount >= BankDatabase.getSpendingLimit(customer.getAccountNumber()) || amount > BankDatabase.getAvailableBalance(customer.getAccountNumber())) {
	    			setError("You can not withdrawl that much from your account!", STAGES.Withdrawl);
	    		} else { 
	    			dispenser.removeCash(amount);
	    			BankDatabase.withdrawlFunds(customer.getAccountNumber(), amount);
	    			withdrawlAmount += amount;
	    			generatePrintMessage();
	    			setStage(STAGES.PrintPrompt);
	    		}
	    		break;
	    	case Deposit:
	    		this.depositAmount = Integer.parseInt(input);
	    		if(depositAmount == 0) {
	    			setStage(STAGES.UserMain);
	    			break;
	    		} else {
	    			depositCash();
	    			generatePrintMessage();
		    		setStage(STAGES.PrintPrompt);
		    		break;
	    		}
	    	case PrintPrompt:
	    		setStage(STAGES.UserMain);
    	}
    }

    /* This function sets the program to a specified stage */
    public void setStage(STAGES state) {
    	clearInput();
    	switch (state) {
	    	case Welcome:
	    		customer = null;
	    		withdrawlAmount = 0;
	    		setScreenText("Welcome to Credit Union\nPress the Enter button to continue");
	    		this.accountNumber = 0;
	    		this.pin = 0;
	    		setReadState(false);
	    		break;
	    	case Account:
	    		setScreenText("Please enter your account number: ");
	    		setReadState(true);
	    		break;
	    	case PIN:
	    		setScreenText("Please enter your PIN: ");
	    		setReadState(true);
	    		break;
	    	case UserMain:
	    		printMenu();
	    		setReadState(true);
	    		break;
	    	case Balance:
	    		showBalance();
	    		setReadState(false);
	    		break;
	    	case Withdrawl:
	    		printWithdrawlMenu();
	    		setReadState(true);
	    		break;
	    	case Deposit:
	    		printDepositMenu();
	    		setReadState(true);
	    		break;
	    	case Error:
	    		setReadState(false);
	    		break;
	    	case PrintPrompt:
	    		displayPrintMessage();
	    		setReadState(false);
	    		break;
    	}
    	currentStage = state;
    }
    
    /* This function signs the user out */
    public void exitSession() {
    	setStage(STAGES.Welcome);
    }
    
    /* This function displays an error message to the user */
    public void setError(String message) {
    	setError(message, STAGES.Welcome);
    }
    
    /* This function displays an error message to the user and takes them to
     * the specified stage.
     */
    private void setError(String message, STAGES nextStage) {
    	setStage(nextStage);
    	setScreenText(message + "\nPress Enter to continue.");
    }
    
    
    /* Transactions */
    private void showBalance() {
		StringBuffer balanceInfo = new StringBuffer();
		
		setScreenText("Your total balance is: " + BankDatabase.getBalance(customer.getAccountNumber()) +
					  "\nYour available balance is: " + BankDatabase.getAvailableBalance(customer.getAccountNumber()));
						
		balanceInfo .append("Transaction: BALANCE INQUIRY")
					.append("\nCurrent Balance: $" + BankDatabase.getBalance(customer.getAccountNumber()))
					.append("\nAvailable Balance: $" + BankDatabase.getAvailableBalance(customer.getAccountNumber()));
	
		allowPrinting = true;
		
		return;
	}
    
    private void depositCash() {
		try {
			int amount = Integer.parseInt(input);
			BankDatabase.depositFunds(customer.getAccountNumber(), amount);
			
		} catch (RuntimeException e) {
			System.out.println("That was not a valid amount.");
		}
    }
    
    /* Setters & Getters */
    private void setReadState(boolean newState) {
    	readingInput = newState;
    }
    
    public STAGES getStage() { return currentStage; }
    
    /* Misc */
    /* Determines if the ATM is reading user input */
    public boolean isReading() {
    	return readingInput;
    }
    
    /* This function generates an appropriate transaction message to be printed
     * based on the current stage of the program
     */
    private void generatePrintMessage() {
    	StringBuilder info = new StringBuilder();
    	switch(currentStage) {
		case Balance:
			info.append("BALANCE INQUIRY");
			break;
		case Deposit:
			info.append("DEPOSIT");
			info.append("\nAmount: $" + Integer.toString(depositAmount));
			break;
		case Withdrawl:
			info.append("WITHDRAWL");
			info.append("\nAmount: $" + Integer.toString(withdrawlAmount));
			break;
			default: return;
		}
    	printMessage = info.toString();
    }
    
    /* Prompts the user to print a reciept */
    private void displayPrintMessage() {
    	allowPrinting = true;
    	switch(currentStage) {
    		case Balance:
    			/* The screen shouldn't be cleared */
    			appendText("\nPress the Print button to print a reciept or Enter to go back to the main menu");
    			break;
	    	case Deposit:
	    		setScreenText("You have successfully deposited " + depositAmount + " to your account." +
	    					  "\nPress the print button to print a recipt or Enter to go back to the main menu.");
	    		//printMessage = new String("")
	    		break;
	    	case Withdrawl:
	    		setScreenText("You have successfully withdrawn " + withdrawlAmount + " from your account." +
	    					  "\nPress the print button to print a reciept or Enter to go back to the main menu.");
	    		break;
			default:
				break;
    	}
    }

    /* Prints a reciept by writing to a file */
    public void printReciept() {
    	if(customer == null || !allowPrinting) return;
    	
    	/* Create a new String with a generic header and a message as specified */
		StringBuffer info = new StringBuffer();
		FileWriter writer;
		
		info.append(customer.getFullName())
			.append("\nAccount Number: " + customer.getAccountNumber())
			.append("\nTransaction: " );
		
		
		info.append(printMessage);
		info.append("\nCurrent Balance: $" + BankDatabase.getAvailableBalance(customer.getAccountNumber()));
		info.append("\nTotal Balance: $" + BankDatabase.getBalance(customer.getAccountNumber()));
		
		try {
			/* Specifies to append to the file if it exists */
			writer = new FileWriter("receipt.txt", true);
			writer.write(info.toString());
			
			/* Gives better spacing for the text file */
			writer.write("\n\n");
			
			writer.flush();
			writer.close();
		} catch (IOException ioe) {
			setScreenText("It seems we couldn't print your reciept. If you have any questions call customer support.");
		}
		allowPrinting = false;
    }
    
    /* This function is used to determine the amount of money to be withdrawn
     * based on user input in the withdrawl menu
     */
    private int parseWithdrawlAmount(int choice) {
    	switch (choice) {
    	
    	case 1:
    		return 20;
    	case 2:
    		return 50;
    	case 3:
    		return 70;
    	case 4:
    		return 100;
    	case 5:
    		return 150;
    	case 6:
    		return 200;
    		default: return 0;
    	}
    	
    }
   
    /* This clears the underlying input buffer. This is called whenever there
     * is a change of stage.
     */
    public void clearInput() {
    	input = new String("");
    }
    

    /* Key Events */
    public void keyPressed(KeyEvent event) {
    	if(isReading()) {
    		switch(event.getKeyCode()) {
		    	case KeyEvent.VK_NUMPAD1:
		    		appendText(Integer.toString(1));
		    		input.concat(Integer.toString(1));
		    		break;
		    	case KeyEvent.VK_NUMPAD2:
		    		appendText(Integer.toString(2));
		    		input.concat(Integer.toString(2));
		    		break;
		    	case KeyEvent.VK_NUMPAD3:
		    		appendText(Integer.toString(3));
		    		input.concat(Integer.toString(3));
		    		break;
		    	case KeyEvent.VK_NUMPAD4:
		    		appendText(Integer.toString(4));
		    		input.concat(Integer.toString(4));
		    		break;
		    	case KeyEvent.VK_NUMPAD5:
		    		appendText(Integer.toString(5));
		    		input.concat(Integer.toString(5));
		    		break;
		    	case KeyEvent.VK_NUMPAD6:
		    		appendText(Integer.toString(6));
		    		input.concat(Integer.toString(6));
		    		break;
		    	case KeyEvent.VK_NUMPAD7:
		    		appendText(Integer.toString(7));
		    		input.concat(Integer.toString(7));
		    		break;
		    	case KeyEvent.VK_NUMPAD8:
		    		appendText(Integer.toString(8));
		    		input.concat(Integer.toString(8));
		    		break;
		    	case KeyEvent.VK_NUMPAD9:
		    		appendText(Integer.toString(9));
		    		input.concat(Integer.toString(9));
		    		break;
		    	case KeyEvent.VK_NUMPAD0:
		    		appendText(Integer.toString(0));
		    		input.concat(Integer.toString(0));
		    		break;
	    		default:
	    			return;
    		}
    	} else {
    		
    	}
    }
    
    /* Empty Events */
    
    /* These events are empty because they are not used, but are needed
     * by the KeyListener interface
     */
    public void keyReleased(KeyEvent event) {
    	/* Do Nothing */
    }
    public void keyTyped(KeyEvent event) {
    	/* Do Nothing */
    }
    
    /* Screen manipulation */
    public String getScreenText()
    {
        return screen.getText();
    }
    
    public void setScreenText(String text)
    {
        screen.setText(text);
    }
    
    /* Appends text to the screen. This is used to display the user's input
     * on screen
     */
    public void appendText(String text) {
    	screen.setText(screen.getText()  + text);
    	input = input.concat(text);
    }

    /* Entry point; creates a new frame and stuff */
    public static void main(String[] args) 
    {
        ATM atmApp = new ATM();
    }
}

class ButtonsPanel extends JPanel
{
    private ATM atm;    // will be used to acces the screen
    private JButton oneJButton, twoJButton,
      threeJButton, fourJButton, fiveJButton, sixJButton,
      sevenJButton, eightJButton, nineJButton, zeroJButton;
    // array of JButtons
   private JButton[] keyJButtons =  new JButton[11];

   public  ButtonsPanel(ATM a)
   {
       atm = a;
       
       setLayout(new GridLayout(4, 3, 5, 10));
       setBackground(Color.GRAY);
        setCursor(new Cursor(Cursor.HAND_CURSOR));
       
      oneJButton = new JButton("1");
      oneJButton.setBackground(Color.LIGHT_GRAY);
      oneJButton.setBorder(new BevelBorder(BevelBorder.RAISED));
      add( oneJButton );
      keyJButtons[1] = oneJButton;

      // set up twoJButton
      twoJButton = new JButton("2");
      twoJButton.setBackground(Color.LIGHT_GRAY);
      twoJButton.setBorder(new BevelBorder(BevelBorder.RAISED));
      add( twoJButton );
      keyJButtons[2] = twoJButton;

      // set up threeJButton
      threeJButton = new JButton( "3" );
      threeJButton.setBackground(Color.LIGHT_GRAY);
      threeJButton.setBorder(new BevelBorder(BevelBorder.RAISED));
      add( threeJButton );
      keyJButtons[3] = threeJButton;

      // set up fourJButton
      fourJButton = new JButton( "4" );
      fourJButton.setBackground(Color.LIGHT_GRAY);
      fourJButton.setBorder(new BevelBorder(BevelBorder.RAISED));
      add( fourJButton );
      keyJButtons[4] = fourJButton;

      // set up fiveJButton
      fiveJButton = new JButton( "5" );
      fiveJButton.setBackground(Color.LIGHT_GRAY);
      fiveJButton.setBorder(new BevelBorder(BevelBorder.RAISED));
      add( fiveJButton );
      keyJButtons[5] = fiveJButton;

      // set up sixJButton
      sixJButton = new JButton( "6" );
      sixJButton.setBackground(Color.LIGHT_GRAY);
      sixJButton.setBorder(new BevelBorder(BevelBorder.RAISED));
      add( sixJButton );
      keyJButtons[6] = sixJButton;

      // set up sevenJButton
      sevenJButton = new JButton( "7" );
      sevenJButton.setBackground(Color.LIGHT_GRAY);
      sevenJButton.setBorder(new BevelBorder(BevelBorder.RAISED));
      add( sevenJButton );
      keyJButtons[7] = sevenJButton;

      // set up eightJButton
      eightJButton = new JButton( "8" );
      eightJButton.setBackground(Color.LIGHT_GRAY);
      eightJButton.setBorder(new BevelBorder(BevelBorder.RAISED));
      add( eightJButton );
      keyJButtons[8] = eightJButton;

      // set up nineJButton
      nineJButton = new JButton( "9" );
      nineJButton.setBackground(Color.LIGHT_GRAY);
      nineJButton.setBorder(new BevelBorder(BevelBorder.RAISED));
      add( nineJButton );
      keyJButtons[9] = nineJButton;

      // set up zeroJButton
      zeroJButton = new JButton( "0" );
      zeroJButton.setBackground(Color.LIGHT_GRAY);
      zeroJButton.setBorder(new BevelBorder(BevelBorder.RAISED));
      add( zeroJButton );
      keyJButtons[0] = zeroJButton;   
      
      for(int i = 0; i < 10; i++) {
    	  keyJButtons[i].addActionListener(new DigitButtonListener());
      }
      
   }  
   
   /* This class listens for the digit buttons to be clicked by the user */
   private class DigitButtonListener implements ActionListener {
	   public void actionPerformed(ActionEvent event) {
		   JButton button = (JButton) event.getSource();
		   if(atm.isReading()) 
			   atm.appendText(button.getText());
	   }
   }
   
   
}

class ControlPanel extends JPanel
{
    private ATM atm;    // will be used to acces the screen
    private JButton enterButton, clearButton, printButton, exitButton;

   public  ControlPanel(ATM a)
   {
       atm = a;
      
      setLayout(new GridLayout(4, 1, 5, 10));
      setBackground(Color.GRAY);
      setCursor(new Cursor(Cursor.HAND_CURSOR));
        
       // set up enterButton
      enterButton = new JButton( "Enter" );
      enterButton.addActionListener(new EnterButtonListener());
      add( enterButton );
      
      clearButton = new JButton( "Clear" );
      clearButton.addActionListener(new ClearButtonListener());
      add( clearButton );
      
      exitButton = new JButton( "Exit" );
      exitButton.addActionListener(new ExitButtonListener());
      add( exitButton );
      
      printButton = new JButton( "Print" );
      printButton.addActionListener(new PrintButtonListener());
      add( printButton );
   }
   
   /* These classes listen for their respective command buttons to be
    * clicked by the user
    */
   private class EnterButtonListener implements ActionListener {
	   public void actionPerformed(ActionEvent event) {
		   // Change the stage
		   atm.advanceStage();
	   }
   }
   
   private class ExitButtonListener implements ActionListener {
	   public void actionPerformed(ActionEvent event) {
		   /* Goes back to the Welcome Message */
		   atm.exitSession();
	   }
   }
   
   private class ClearButtonListener implements ActionListener {
	   public void actionPerformed(ActionEvent event) {
		   // Clear the screen?
		   atm.clearInput();
		   atm.setStage(atm.getStage());
	   }
   }
   
   private class PrintButtonListener implements ActionListener {
	   public void actionPerformed(ActionEvent event) {
		   // Print a reciept
		   atm.printReciept();
	   }
   }
   
   /* This gives the ActionListeners the ability to access the ATM object */
   public ATM getFrame() { return atm; }
}