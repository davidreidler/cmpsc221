/*******************************************************************************
 * Author:      David Reidler                                                  *
 * Email:       dir5187@psu.edu                                                *
 * Assignment:  2                                                              *
 * Description: This is the test cases which test the functionality of both    *
 * implementations of the DropoutStack, namely LDropouStack and CDropoutStack. *
 * It has been attempted to test each method in each class with different      *
 * corner cases. If the output seems confusing, make sure to check the test    *
 * cases to make sure that everything does work as intended.                   *
 ******************************************************************************/ 

public class DropoutStackTest {
	public static void main(String[] args){
		System.out.println("Beginning test cases for DropoutStack");
		System.out.println("Testing with linear array implementation...");

		LDropoutStack<Integer> s1 = new LDropoutStack<Integer>();
		LDropoutStack<Integer> s2 = new LDropoutStack<Integer>(3);

		System.out.println("Created two empty stacks s1 and s2.");

		/* Test Stack Functionality */
		Integer i = s1.peek(); 

		/* Make sure constructor works correctly */
		if(i == null && s1.empty())
			System.out.println("Success, s1 is empty right now.");		

		/* Testing push() and peek() */
		s1.push(2);

		i = s1.peek();

		if(i == null && s1.empty())
			System.out.println("Something went wrong, s1 is empty");
		else System.out.printf("Successfully pushed: %d\n", i);

		/* Testing pop() with a partially filled stack */
		System.out.println("Pushing 3, 77, 100 to s1.");

		s1.push(3);
		s1.push(77);
		s1.push(100);

		System.out.printf("Size of the stack is: %d\n", s1.size());

		while(!s1.empty()){
			System.out.printf("Popping: %d\n", s1.pop());
		}

		System.out.printf("Size of the stack after popping is %d\n", s1.size());

		/* Checking for Dropout functionality */
		System.out.println("\nTesting Dropout Functionality:");
		System.out.println("s2 has a capacity of 3");
		System.out.println("Pushing 2, 3, 77, and 100 to s2");

		s2.push(2); 
		s2.push(3);
		s2.push(77);
		s2.push(100);

		while (!s2.empty())
			System.out.printf("Popping: %d\n", s2.pop());

		System.out.println("s2 is now empty.");	

		/* Testing resize functionality */
		System.out.println("\nTesting Resize Functionality: ");

		s1.push(0);
		s1.push(1);
		s1.push(2);
		s1.push(3);
		s1.push(4);
		s1.push(5);
		s1.push(6);

		System.out.println("Resizing the stack to a new cap of 3");
		s1.resize(3);

		System.out.println("Resizing the stack to a new cap of 5");
		s1.resize(5);

		System.out.printf("The size of s1 is %d\n", s1.size());
		
		if(s1.peek() == null)
			System.out.printf("The last element is null!.");

		/* This should pop the last 3 elements */
		while (!s1.empty())
			System.out.printf("Popping: %d\n", s1.pop());

		System.out.println("s1 is now empty.");

		/* Testing Circular Array */
		System.out.println("\nTesting Circular Array implementation...");
		CDropoutStack<Integer> s3 = new CDropoutStack<Integer>(3);

		System.out.println("Pushing 2 to s3");
		s3.push(2);

		System.out.printf("The first addition is: %d\n", s3.peek());

		System.out.println("Pushing 3, 77, and 100");
		s3.push(3);
		s3.push(77);
		s3.push(100);

		System.out.printf("The fourth addition is: %d\n", s3.peek());

		System.out.println("Pushing 200, 88, 99, 23234");
		s3.push(200);
		s3.push(88);
		s3.push(99);
		s3.push(23234);

		System.out.printf("The size is: %d\n", s3.size());

		while(!s3.empty())
			System.out.printf("Popping: %d\n", s3.pop());

		System.out.printf("The size after popping is: %d\n", s3.size());

		System.out.println("\nPushing 2, 3, 77, 100, 200");

		s3.push(2);
		s3.push(3);
		s3.push(77);
		s3.push(100);
		s3.push(200);

                
		System.out.printf("Size is %d\n", s3.size());

		System.out.println("Resizing the stack to a new cap of 4");
		s3.resize(4);

		System.out.println("Pushing 88 and 2342");
		s3.push(88);
		s3.push(2342);

		System.out.printf("New Size is: %d\n", s3.size());
		while(!s3.empty())
			System.out.printf("Popping: %d\n", s3.pop());

		System.out.println("Increasing the stack to 100 elements...");

		s3.resize(100);

		System.out.println("Pushing all numbers from 0 to 99");
		for(int j=0; i < 100; i++)
			s3.push(i);

		System.out.println("Pushed 100 elements to the stack.");

		System.out.println("Resizing to 3 elements");

		s3.resize(3);

		System.out.println("Emptying out the stack...");

		while(!s3.empty())
			System.out.printf("Popping: %d\n", s3.pop());

		System.out.println("The stack is now empty.");
		System.out.println("Test cases completed. Exiting program.");
		return;
	}
}
