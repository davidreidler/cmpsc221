/*******************************************************************************
 * Author:      David Reidler                                                  *
 * Email:       dir5187@psu.edu                                                *
 * Assignment:  2                                                              *
 * Description: This file contains the implementation of a DropoutStack using  *
 * a circular array. This specific implementation however changes start        *
 * whenever the head "loops" around to the start of the array.                 *
 ******************************************************************************/

public class CDropoutStack <E> {
	private static final int DEFAULT_CAPACITY = 15;

	/* Indicates the max number of elements */
	private int capacity;

	/* Indicates the index of the next item to be inserted */
	private int head;

	/* Indicates the index of the first element */
	private int start;

	/* Indicates the number of elements actually stored in the stack */
	private int size;

	private E[] stack;

	public CDropoutStack(){
		this(CDropoutStack.DEFAULT_CAPACITY);
	}

	public CDropoutStack(int c){
		if(c > 0)
			capacity = c;
		else capacity = DEFAULT_CAPACITY;

		head = 0;
		start = 0;
		size = 0;
		stack = (E[]) new Object[capacity];
	}

	public boolean empty(){
		return (size == 0);
	}

	public E peek(){ 
		if(head == 0)
			return (E) stack[capacity - 1];
		else return (E) stack[(head - 1) % capacity];
	}

	public E pop(){
                if(empty()) return null;
                
		/* Modify head */
		if(head == 0)
			head = capacity - 1;
		else head = (head - 1) % capacity;
            
		/* Copy and delete reference */
		E tmp = (E) stack[head];
		stack[head] = null;
		size--;

		return tmp;
	}

	/* Returns the removed object */
	public E push(E newItem){
		if(newItem == null) return null;

		/* Insert new value and increase head */
		E removed = stack[head];
		stack[head] = newItem;
                
                /* This indicates that head cycled through back to start */
                if(head == start)
			start = (start + 1) % capacity;
                
		head = (head + 1) % capacity;

		/* Size can only be as large as capacity */
		if(size != capacity)
			size++;

		return removed;
	}

	public int size(){
		return size;
	}

	public void resize(int newCapacity){
		E[] newStack = (E[]) new Object[newCapacity];

		/* The array should fit completely into the new array */
		if(size <= newCapacity) {
                	stack = linearize(newCapacity); 
                	capacity = newCapacity;

		/* We need to copy newCap elements starting newCap elements 
 		 * before head
 		 */
		} else {
			int k = (head == 0) ? capacity - 1 : head - 1;
			for(int i = newCapacity - 1; i >= 0; i--) {
				newStack[i] = stack[k];
				k = (k == 0) ? capacity - 1 : k - 1;	
			}
			size = newCapacity;
			capacity = newCapacity;
                        stack = newStack;
		} 
		return;
	}	

	private E[] linearize(int newCapacity) {
		/* No reason to call on empty stack */
		if(empty()) return (E[]) new Object[newCapacity];

		E[] result = (E[]) new Object[newCapacity];

		int k = start;
		for(int i = 0; i < newCapacity; i++) {
			result[i] = stack[k];
			k = (k + 1) % capacity;

			if(k == start) break;
		}
                
                /* Reset start and size */
                start = 0;
                head = (size == newCapacity) ? 0 : size;
		return result;
	}
}
