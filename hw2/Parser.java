/*******************************************************************************
 * Author:      David Reidler                                                  *
 * Email:       dir5187@psu.edu                                                *
 * Assignment:  2                                                              *
 * Description: This is a basic parser that determines if a string entered by  *
 * the user containing ( and ) is correctly matched. Internally, the program   *
 * uses a stack that will push ( when one is encountered and pop a ( whenever  *
 * a ) is encountered. Once the string is read, an empty stack will determine  *
 * if the string is correctly matched.                                         *
 ******************************************************************************/

import java.util.Scanner;
import java.util.Stack;

public class Parser {
	private static String testLine;
	private static Scanner inputScanner;

	public static void main(String[] args) {
		inputScanner = new Scanner(System.in);

		System.out.println("Enter the line to be parsed: ");

		testLine = inputScanner.nextLine();

		if(properlyMatched())
			System.out.println("The input string is properly matched.");
		else System.out.println("The input string is ill matched.");

		return;
	}

	private static boolean properlyMatched() {
		/* Iterate over each character and push/pop when needed */
		Stack stack = new Stack();
		if(testLine == null) return false;

		char[] input = testLine.toCharArray();

		for(int i = 0; i < input.length; i++) {
			if(input[i] == '(') {
				stack.push('(');
				continue;
			}

			if(input[i] == ')' && stack.empty())
				return false;
			else if(input[i] == ')') {
				stack.pop();
				continue;
			}
			else continue;

		}

		return stack.empty();
	}
} /* End class Parser */
