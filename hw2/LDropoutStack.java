/*******************************************************************************
 * Author:      David Reidler                                                  *
 * Email:       dir5187@psu.edu                                                *
 * Assignment:  2                                                              *
 * Description: This is a DropoutStack implementation which uses a linear      *
 * array for storage. Basically, if an element is added to a full stack, the   *
 * very bottom element within the stack will be removed from and each element  *
 * is pushed downwards.                                                        *
 ******************************************************************************/

import java.util.Arrays; /* Used for copying arrays */

public class LDropoutStack<E> {
	private static final int DEFAULT_CAPACITY = 15;

	/* Data for objects */
	private int capacity;

	private int size;

	private E[] stack;

	/* Default - create stack with a capacity of 15 elements */
	public LDropoutStack(){
		this(LDropoutStack.DEFAULT_CAPACITY);
	}

	public LDropoutStack(int c) {
		size = 0;

		/* Make sure capacity is positive! */
		if(c > 0)
			capacity = c;
		else capacity = DEFAULT_CAPACITY;
		stack = (E[]) new Object[c];
	}

	/* Returns whether or not the stack is empty */
	public boolean empty(){
		return size == 0;
	}

	/* Returns the element on the top of the stack but doesn't remove it from
	 * the stack
	 */
	public E peek(){
		if(size == 0)
			return null;
		else return (E) stack[size - 1];
	}
	
	/* Returns the element on the top of the and removes it from the
	 * stack
	 */
	public E pop(){
		if(size == 0) return null;

		/* Save and remove reference from stack then return the reference */
		E tmp = (E) stack[size - 1];
		stack[size - 1] = null;
		size--;
		return tmp;

	}

	/* It returns the element that was removed from the stack if the stack
	 * overflowed
	 */
	public E push(E newItem){

		/* A null object should not be added to the stack */
		if(newItem == null) return null;

		E tmp = null;
		if(size == capacity){

			/* Remove the bottom of the stack and shift everything right */
			tmp = (E) stack[0];	

			for(int i = 0; i < capacity - 1; i++){
				stack[i] = stack[i + 1];
			}

			stack[capacity - 1] = newItem;

		} else {
			stack[size] = newItem;
			size++;
		}

		return tmp;
	}

	public int size(){
		return size;
	}

	public void resize(int newCapacity){
		if(newCapacity < capacity && newCapacity < size){
			/* Discard items from bottom of the stack until everything fits */
			stack = Arrays.copyOfRange(stack, size - newCapacity, size);

			capacity = newCapacity;
			size = newCapacity;
		} else {
			/* Copy old to a new resized array - size stays the same */
			stack = Arrays.copyOf(stack, newCapacity);
			capacity = newCapacity;
		}

		return;
	}
}
