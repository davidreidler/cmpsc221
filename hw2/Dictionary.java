/*******************************************************************************
 * Author:      David Reidler                                                  *
 * Email:       dir5187@psu.edu                                                *
 * Assignment:  2                                                              *
 * Description: This is a simple program that allows the user to add and find  *
 * words within a dictionary, and output the entire dictionary in alphabetical *
 * order. Internally, the dictionary stores the words in a Hashtable which     *
 * maps strings to word objects. The Word class and its implementation can be  *
 * found in Word.java. The alphabetical sorting is done through the sort() in  *
 * the collection class.                                                       *
 ******************************************************************************/

import java.util.Scanner;
import java.util.HashMap;
import java.util.*;

public class Dictionary{
	/* Scanner reference for input */
	private static Scanner inputScanner;

	/* HashMap to store words and definitions */
	private static HashMap<String, Word> dictionary;

	public static void main(String[] args){
		inputScanner = new Scanner(System.in);
		dictionary = new HashMap<String, Word>();

		String choice;

		int c = 0;

		System.out.println("Welcome to the dictionary program!");
		System.out.println("Here are your options...\n"); 

		while(c != 4) {
			writeMenu();
			System.out.println("\nMake your selection.");

			choice = inputScanner.nextLine();

			/* TODO: Parse choice into c */
			try {
				c = Integer.parseInt(choice);
				getSelection(c);
			} catch (Exception e) {
				/* Set to 0 to make sure the error is caught */
				System.out.println("Invalid choice");
				c = 0;
			} 
		}
	} 


	/* Static Methods */

	/* Searches for a word in the hash map */
	private static void findWord(String input) {
		Word word = null;

		if(input.contains(" ")){
			System.out.println("That's an invalid word!");
			return;
		}

		word = dictionary.get(input);

		if(word == null)
			System.out.println("That word doesn't exist!");
		else System.out.println("The definition is " + word.getDefinition());

	} /* End findWord() */

	/* Adds a new definition for the word in the hashmap */
	private static void addWord(String word) {
		if(word.contains(" ")) {
			System.out.println("That's an invaid word!");
			return;
		} else {
			if(dictionary.get(word) != null) {
				System.out.println("Overwrite current definition? (y/n)");
				String answer = inputScanner.nextLine();
				if(!answer.equalsIgnoreCase("y")) return;
			}
			Word newWord = new Word(word);
			System.out.println("Enter the definition for your new word.");
			newWord.setDefinition(inputScanner.nextLine());

			dictionary.put(word, newWord);
		}

		return;
	} /* End addWord() */

	/* Displays all words in the dictionary */
	private static void  displayAllWords() {
		List<Word> words = new ArrayList<Word>(dictionary.values());

		Collections.sort(words);
		for(Word w : words) {
			System.out.println(w.getWord() + "-" + w.getDefinition());
		}
		return; 
	} /* End displayAllWords() */
	private static void getSelection(int c) {
		switch(c){
			case 0: break;
			case 1:
				System.out.println("Enter the word you're looking for.");
				findWord(inputScanner.nextLine());
				break;

			case 2:
				System.out.println("Enter the word you would like to add.");
				addWord(inputScanner.nextLine());
				break;

			case 3:
				System.out.println("Here are the words in the dictionary.");
				displayAllWords();
				break;

			case 4:
				System.out.println("Exiting program");
				return;

			default:
				System.out.println("Invalid Choice");
		}
	}
	/* Displays the menu */
	private static void writeMenu() {
		System.out.println("\n1) Search the dictionary");
		System.out.println("2) Add a word to the dictionary");
		System.out.println("3) Display all words in the dictionary");
		System.out.println("4) Exit the program");
		return;
	} /* End writeMenu() */
} /* End class Dictionary */
