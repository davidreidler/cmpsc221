/*******************************************************************************
 * Author:      David Reidler                                                  *
 * Email:       dir5187@psu.edu                                                *
 * Assignment:  2                                                              *
 * Description: This file contains the implementation of the Word data type.   *
 * This class implements Comparable<Word> allowing it to be sorted by          *
 * Collection.sort(). Internally, compareTo() returns the value of compareTo() *
 * on the "word" member of two objects. This makes the Dictionary class logic  *
 * much more readable and understandable.                                      *
 ******************************************************************************/

public class Word implements Comparable<Word> {
	/* Word for which we're defining */
	private String word;

	/* Definition of a Word */
	private String definition;


	/* Creates a new word without a definition */
	public Word(String word) {
		this.word = new String(word);
	}

	public Word(String word, String definition) {
		/* Deep copy to make sure reference lives the same as the object */
		this.word = new String(word);
		this.definition = new String(definition);
	} /* End constructor(String, String) */


	/* Getters */
	public String getWord() {
		return word;
	} /* End getWord() */

	public String getDefinition() {
		return definition;
	} /* End getDefinition() */


	/* Setters */
	public void setWord(String word) {
		if(word != null && !word.equals(""))
			this.word = new String(word);
		else return;
	} /* End setWord() */

	public void setDefinition(String def) {
		if(def != null && !def.equals("")){
			this.definition = new String(def);
		}
		else return;
	} /* End setDefinition() */

	public int compareTo(Word w) {
		return this.word.compareTo(w.word);
	}
} /* End class Word */
