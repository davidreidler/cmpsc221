/******************************************************************************
 * BraceConverter.java                                                        *
 *                                                                            *
 * Author: 		David Reidler                                                 *
 * E-Mail:		dir5187@psu.edu                                               *
 * Assignment: 	4                                                             *
 * Description: This file contains java style brace converter. This program   *
 * takes an input java file name and converts the java file from end-of-line  *
 * brace style to new-line brace style.                                       *
 *****************************************************************************/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class BraceConverter {
	/* Name of the file to read/write to and from */
	private static String filename;
	
	public static void main(String[] args) {
		/* Make sure there's a file */
		if(args.length < 1) {
			System.out.println("You must specify a filename!");
			return;
		}

		filename = args[0];
		
		/* Read file */
		String[] lines = readFile();
		String[] newFile = new String[lines.length];
		StringBuilder sb;
		String s;
		
		int newFileIndex = 0;
		for(int i = 0; i < lines.length; i++) {
			s = lines[i];
			
			/* This line didn't have a '{', just add it to the file */
			if(s.indexOf('{') == -1) {
				sb = new StringBuilder(s);
				sb.append("\r\n");
				newFile[newFileIndex] = sb.toString();
				newFileIndex++;
				continue;
			}
			
			/* Gets the white space of the current line */
			String whiteSpace = readWhiteSpace(s);
						
			sb = new StringBuilder(s);
			
			/* Get the insertion point */
			int insertionPoint = sb.indexOf("{");
			
			/* Deletes the space before '{' */
			sb.deleteCharAt(insertionPoint - 1);
			
			/* Insert a newline and whitespace before the '{' */
			insertionPoint = sb.indexOf("{");
			sb.insert(insertionPoint, "\r\n" + whiteSpace);
			
			insertionPoint = sb.indexOf("{") + 1;
			sb.insert(insertionPoint, "\r\n");
			
			
			/* Save the new string into the array */
			newFile[newFileIndex] = sb.toString();
			newFileIndex++;
		}
		
		/* Output the file */
		saveFile(newFile);
	}
	
	/* Reads the file and returns the lines as a String[] */
	private static String[] readFile() {
		ArrayList<String> res = new ArrayList<String>();
		String[] result; 
		BufferedReader reader;
		String s;
		
		try {
			/* Read the file */
			reader = new BufferedReader(new FileReader(filename));
			while((s = reader.readLine()) != null) {
				res.add(s);
			}
			
			reader.close();
			
			/* Return the result */
			result = new String[res.size()];
			for(int i = 0; i < res.size(); i++) {
				result[i] = res.get(i).toString();
			}
			return result;
			
		} catch (IOException ioe) {
			System.out.println("Could not read the file");
			return null;
		}
	}
	
	/* Reads the line and returns the amount of whitespace */
	private static String readWhiteSpace(String line) {
		StringBuffer resBuff = new StringBuffer();
		
		char[] chars = line.toCharArray();
		
		for(int i = 0; i < line.length(); i++) {
			if(chars[i] == ' ' || chars[i] == '\t') {
				resBuff.append(chars[i]);
			} else break;
		}
		
		return resBuff.toString();
	}
	
	/* Takes the String[] and writes the file to disk */
	private static void saveFile(String[] newFile) {
		FileWriter writer;
		
		/* Write the file */
		try {
			writer = new FileWriter(filename, false);
			for(String str : newFile) {
				writer.write(str);
			}

			
			writer.flush();
			writer.close();
			return;
		} catch (IOException ioe) {
			System.out.println("Could not write the file.");
			return;
		}
	}
}
