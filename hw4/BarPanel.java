/******************************************************************************
 * BarPanel.java                                                              *
 *                                                                            *
 * Author: 		David Reidler                                                 *
 * E-Mail:		dir5187@psu.edu                                               *
 * Assignment: 	4                                                             *
 * Description: This file contains the implementation for the bar drawing     *
 * panel. This is where the bar chart drawing is done within the window.      *
 *****************************************************************************/

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class BarPanel extends JPanel {
	/* Number of bars drawn */
	private int numberOfBars = 0;
	
	/* Course Color Constants */
	private final Color PROJECTS = Color.RED;
	private final Color QUIZZES  = Color.BLUE;
	private final Color MIDTERMS = Color.GREEN;
	private final Color FINAL    = Color.ORANGE;
	
	/* Course Percentages */
	private final int PROJECT_PERCENTAGE = 20;
	private final int QUIZ_PERCENTAGE    = 10;
	private final int MIDTERM_PERCENTAGE = 30;
	private final int FINAL_PERCENTAGE   = 40;
	
	/* Course Labels */
	private final String PROJECT_LABEL = "Projects -- 20%";
	private final String QUIZ_LABEL    = "Quizzes -- 10%";
	private final String MIDTERM_LABEL = "Midterms -- 30%";
	private final String FINAL_LABEL   = "Final -- 40%";
	
	/* Pixels per Percent */
	private final int PIXEL_PERCENT = 5;
	
	/* Spacing between bars */
	private final int SPACING = 20;
	
	/* Width of a bar */
	private final int BAR_WIDTH = 100;

	/* Number of pixels between top of bar and label */
	private final int STRING_OFFSET = 5;
	
	/* Pixels to start drawing bars at */
	private final int BAR_START = 10;
	
	public BarPanel() {
		super();
		setBackground(Color.WHITE);
		setSize(500, 300);
		numberOfBars = 0;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		/* Draw the bottom line */
		g.setColor(Color.BLACK);
		g.drawLine(5, 245, 475, 245);
		
		/* Draw the bars */
		drawBar(g, PROJECTS, PROJECT_PERCENTAGE, 1);
		drawBar(g, QUIZZES, QUIZ_PERCENTAGE, 2);
		drawBar(g, MIDTERMS, MIDTERM_PERCENTAGE, 3);
		drawBar(g, FINAL, FINAL_PERCENTAGE, 4);


		/* Draw the Strings */
		g.setColor(Color.BLACK);
		drawString(g, PROJECT_LABEL, PROJECT_PERCENTAGE, 1);
		drawString(g, QUIZ_LABEL, QUIZ_PERCENTAGE, 2);
		drawString(g, MIDTERM_LABEL, MIDTERM_PERCENTAGE, 3);
		drawString(g, FINAL_LABEL, FINAL_PERCENTAGE, 4);
	}
	
	/* Gets how high the bar should be drawn from the line */
	private int yOffset(int percent) {
		return PIXEL_PERCENT * percent;
	}
	
	/* Gets the x distance that is between two bars */
	private int xOffset() {
		return BAR_WIDTH + SPACING;
	}
	
	/* This method will draw the bar based on the percentages and the bar number */
	private void drawBar(Graphics g, Color color, int percentage, int barNumber) {
		g.setColor(color);
		g.fillRect(BAR_START + (barNumber - 1) * xOffset(), 
				   245 - yOffset(percentage), 
				   BAR_WIDTH,
				   yOffset(percentage));
		
	}
	
	/* This method will draw the a string above the specified bar */
	private void drawString(Graphics g, String str, int percentage, int barNumber) {
		g.drawString(str, BAR_START + (barNumber - 1) * xOffset(), 245 - yOffset(percentage) - STRING_OFFSET);
	}
}
