/******************************************************************************
 * BarChart.java                                                              *
 *                                                                            *
 * Author: 		David Reidler                                                 *
 * E-Mail:		dir5187@psu.edu                                               *
 * Assignment: 	4                                                             *
 * Description: This file contains the implementation for the bar chart. It   *
 * will draw a bar chart as specified in BarPanel inside the JFrame as        *
 * within the Assignment specifications.                                      *
 *****************************************************************************/
import javax.swing.JFrame;

public class BarChart {

	public static void main(String[] args) {
		
		/* Set up the main window */
		JFrame frame = new JFrame("Course Overview)");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 300);
		
		/* Create and add the Panel */
		BarPanel panel = new BarPanel();
		frame.add(panel);
		
		
		/* Show the window */
		frame.setVisible(true);
		
	}

}
