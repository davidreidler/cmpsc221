/******************************************************************************
 * IntSearch.java                                                             *
 *                                                                            *
 * Author: 		David Reidler                                                 *
 * E-Mail:		dir5187@psu.edu                                               *
 * Assignment: 	4                                                             *
 * Description: This file contains the implementation for an integer search   *
 * program. It first creates an array of 100 integers and asks the user for   *
 * an index. If the index is within the bounds of the array, it will return a *
 * number. If the index is not within the bounds of the array, it will        *
 * display an error and ask the user to specify a new index.                  *
 *****************************************************************************/

import java.util.Random;
import java.util.Scanner;

public class IntSearch {

	public static void main(String[] args) {
		int[] integers = randomArray();
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enter the index that you would like to access: ");
		
		/* Get the index from the user */
		boolean tryAgain = true;
		while(tryAgain){
			try {
				String s = input.nextLine();
				int choice = Integer.parseInt(s);
				
				System.out.println("That number is: " + integers[choice]);
				tryAgain = false;
			} catch (IndexOutOfBoundsException e) {
				System.out.println("That is not a valid index!");
				System.out.println("Please enter a new index: ");
			}
		}
		return;
			
	}
	
	/* Creates a random array of integers */
	private static int[] randomArray() {
		Random rand = new Random();
		int[] result = new int[100];
		
		/* Create 100 random integers */
		for (int i = 0; i < 100; i++) {
			result[i] = rand.nextInt();
		}
		
		return result;
	}

}
