import ATM.*;

public class Program {

	public static void main(String[] args) {
		ATM atm = new ATM();

		while(!atm.shuttingDown()) {
			atm.run();
		}

		System.out.println("Thank you for using our service.");
		return;
	}
}
