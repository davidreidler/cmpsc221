package ATM;
class BasicAccount extends Account {
	private static final int SPENDING_LIMIT = 1000;

	BasicAccount() {
		super(0000, 1234, 0.0);
	}

	BasicAccount(int accNumber, int pinNumber, double curBalance) {
		super(accNumber, pinNumber, curBalance);
	}

	int spendingLimit() {
		return SPENDING_LIMIT;
	}
}
