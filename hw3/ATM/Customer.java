package ATM;
public class Customer {

	/* Fields */
	private String fullName;

	private String address;

	private String phoneNumber;

	private int accountNumber;

	/* Constructor */
	Customer(String name, String addr, String phone, int accNumber) {
		fullName = new String(name);
		address = new String(addr);
		phoneNumber = new String(phone);
		accountNumber = accNumber;
	}


	/* Getters */
	public int getAccountNumber() {
		return accountNumber;
	}
	
	public String getAddress() {
		return address;
	}

	public String getFullName() {
		return fullName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	/* Setters */
	void setAccountNumber(int newNumber) {
		accountNumber = newNumber;
	}
	
	void setAddress(String newAddr) {
		address = new String(newAddr);
	}

	void setFullName(String newName) {
		fullName = new String(newName);
	}

	void setPhoneNumber(String newNumber) {
		phoneNumber = new String(newNumber);
	}
}
