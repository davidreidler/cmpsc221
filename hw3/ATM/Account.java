package ATM;
abstract class Account {
	/* Fields for each subclass */
	protected int accountNumber;
	protected int pin;

	protected double balance;
	protected double availableBalance;


	/* Basic Constructors */
	protected Account(int accNumber, int pinNumber, double curBalance) {
		accountNumber = accNumber;
		pin = pinNumber;
		balance = curBalance;
		availableBalance = curBalance;
	}


	/* Basic getters */
	int getAccountNumber() { return accountNumber; }
	
	double getAvailableBalance() { return availableBalance; }

	double getBalance() { return balance; }

	int getPin() { return pin; }


	/* Account Operations */
	void depositFunds(double amount) {
		balance += amount;
	}
	
	double withdrawlFunds(double amount) {
		if(amount > balance || amount > spendingLimit())
			return 0.0;

		
		balance -= amount;
		return amount;
	}


	/* Abstract methods */
	abstract int spendingLimit();
}
