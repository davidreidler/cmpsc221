package ATM;
public class StudentAccount extends Account {
	private static final int SPENDING_LIMIT = 200;

	StudentAccount() {
		super(0000, 1234, 0.0);
	}
	
	StudentAccount(int accNumber, int PIN, double balance) {
		super(accNumber, PIN, balance);
	}
	
	int spendingLimit() {
		return SPENDING_LIMIT;
	}

}
