package ATM;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
public class ATM {
	/* Fields */
	private boolean shuttingDown = false;
	private Customer customer;
	private CashDispenser dispenser;
	private Scanner input;
	private int withdrawlAmount = 0;  /* Amount that needs to be verified and written to the database for the current customer */

	/* Constructor */
	public ATM() {
		dispenser = new CashDispenser();
		input = new Scanner(System.in);
	}


	/* Methods */
	public void run() {
		int accountNumber;
		int pinNumber;

		withdrawlAmount = 0;
		
		/* Initialize the database */
		BankDatabase.init();

		printInitialScreen();
		try {
			accountNumber = Integer.parseInt(input.nextLine());
			
			/* The user is exiting and the ATM should shutdown */
			if(accountNumber == 99) {
				shuttingDown = true;
				return;
			}
			
			if(!BankDatabase.accountExists(accountNumber)){ 
				System.out.println("That account does not exist!");
				return;
			}

			printPINPrompt();

			pinNumber = Integer.parseInt(input.nextLine());

			if(!BankDatabase.verifyPIN(accountNumber, pinNumber)){
				System.out.println("Wrong PIN number!");
				return;
			}

			customer = BankDatabase.findCustomer(accountNumber);
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
			return;
		}

		printMenu();	
		
		BankDatabase.saveState();

	}

	private void printInitialScreen() {
		System.out.println("\tWelcome to Credit Union");
		System.out.println("\nEnter Account Number:");
		return;
	}

	private void printPINPrompt() {
		System.out.println("Enter your PIN:");
		return;
	}

	private void printMenu() {
		int choice = 0;
		while(choice != 4 && choice != 99) {
			System.out.println("\tWelcome " + customer.getFullName());
			System.out.println("Please choose an option:");
			System.out.println("\t(1) Balance Inquiry");
			System.out.println("\t(2) Withdrawl Money");
			System.out.println("\t(3) Deposit Money");
			System.out.println("\t(4) Sign Out");
			
			try {
				choice = Integer.parseInt(input.nextLine());

				switch (choice) {
					case 1: showBalance(); break;
					case 2: withdrawlCash(); break;
					case 3: depositCash(); break;
					case 4: break;
					case 99: shuttingDown = true; break;
					default: break;
				}	
			} catch (RuntimeException e) {
				System.out.println("You have entered invalid input.");
			}
		}
	}

	public boolean shuttingDown() {
		return shuttingDown;
	}

	private void showBalance() {
		StringBuffer balanceInfo = new StringBuffer();
		
		System.out.println("Your total balance is: " + BankDatabase.getBalance(customer.getAccountNumber()));
		System.out.println("Your available balance is: " + BankDatabase.getAvailableBalance(customer.getAccountNumber()));
		
		balanceInfo .append("Transaction: BALANCE INQUIRY")
					.append("\nCurrent Balance: $" + BankDatabase.getBalance(customer.getAccountNumber()))
					.append("\nAvailable Balance: $" + BankDatabase.getAvailableBalance(customer.getAccountNumber()));
	
		System.out.println("Would you like a receipt? (Y/N)");
		
		String c = input.nextLine();
		if(c.equalsIgnoreCase("Y"))
			printReceipt(balanceInfo.toString());
		
		return;
	}
	
	private void withdrawlCash() {
		int amount = 0;
		boolean finished = false;
		double available = BankDatabase.getAvailableBalance(customer.getAccountNumber());
		
		while(!finished) {
			System.out.println("How much would you like to withdrawl?");
			System.out.println("(1) $20\t\t(4) $100");
			System.out.println("(2) $50\t\t(5) $150");
			System.out.println("(3) $70\t\t(6) $200");
			System.out.println("(0) Exit");
			
			int choice = Integer.parseInt(input.nextLine());
			switch (choice) {
				case 0: return;
				case 1: amount = 20; break;
				case 2: amount = 50; break;
				case 3: amount = 70; break;
				case 4: amount = 100; break;
				case 5: amount = 150; break;
				case 6: amount = 200; break;
			}
			
			/* Make sure the dispenser has enought left */
			if(dispenser.amountLeft() < amount) {
				System.out.println("We're sorry, the ATM doesn't have that much money in it.");
				break;
			}
			
			/* Make sure that the account has enough to withdrawl */		
			if(withdrawlAmount < BankDatabase.getSpendingLimit(customer.getAccountNumber()) && available > amount) {
				/* Gets the amount removed by the cash dispenser */
				dispenser.removeCash(amount);
				withdrawlAmount += amount;
				
				BankDatabase.withdrawlFunds(customer.getAccountNumber(), amount);
				finished = true;
				
				System.out.println("Please be sure to take the money from the dispenser.");
				System.out.println("Would you like a receipt? (Y/N)");
				
				String c = input.nextLine();
				
				/* Build Transaction message */
				if(c.equalsIgnoreCase("Y")) {
					StringBuffer message = new StringBuffer();
					message.append("Transaction: WITHDRAWL")
							.append("\nAmount: $" + amount)
							.append("\nCurrent Balance: $" + BankDatabase.getBalance(customer.getAccountNumber()));
					
					printReceipt(message.toString());
				}
			} else {
				System.out.println("That amount either exceeds your available balance or your spending limit.");
				System.out.println("Please enter a different amount.");
			}
		}
	}
	
	private void depositCash() {
		/* Stores the transaction message */
		StringBuffer message = new StringBuffer();
		
		System.out.println("Enter the amount you would like to deposit.");
		String s = input.nextLine();
		
		if(s.equals("0")) return;
		try {
			double amount = Double.parseDouble(s);
			
			BankDatabase.depositFunds(customer.getAccountNumber(), amount);
			
			System.out.println("You have successfully depositied " + amount + " into your account.");
			System.out.println("Your new balance is:  $" + BankDatabase.getBalance(customer.getAccountNumber()));
			System.out.println("Would you like a receipt? (Y/N)");
			
			String choice = input.nextLine();
			if(choice.equalsIgnoreCase("Y")) {
				message .append("Transaction: DEPOSIT")
						.append("\nAmount: $" + amount)
						.append("\nTotal Balance: $" + BankDatabase.getBalance(customer.getAccountNumber()));
				
				printReceipt(message.toString());
			} else return;
		} catch (RuntimeException e) {
			System.out.println("That was not a valid amount.");
		}
	}
	
	private void printReceipt(String message) {
		/* Create a new String with a generic header and a message as specified */
		StringBuffer info = new StringBuffer();
		FileWriter writer;
		
		info.append(customer.getFullName())
			.append("\nAccount Number: " + customer.getAccountNumber())
			.append("\n" + message);
		
		try {
			/* Specifies to append to the file if it exists */
			writer = new FileWriter("receipt.txt", true);
			writer.write(info.toString());
			
			/* Gives better spacing for the text file */
			writer.write("\n\n");
			
			writer.flush();
			writer.close();
		} catch (IOException ioe) {
			System.out.println("It seems we couldn't print your reciept. If you have any questions call customer support.");
		}
	}
}
