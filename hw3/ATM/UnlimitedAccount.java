package ATM;
public class UnlimitedAccount extends Account {
	private static final int SPENDING_LIMIT = Integer.MAX_VALUE;

	UnlimitedAccount() {
		super(0000, 1234, 0.0);
	}
	
	UnlimitedAccount(int accNumber, int PIN, double balance) {
		super(accNumber, PIN, balance);
	}
	
	int spendingLimit() {
		// TODO Auto-generated method stub
		return SPENDING_LIMIT;
	}

}
